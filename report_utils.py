import time, random, string
from celery import Celery
import json
from config import params

broker = params["broker"]
backend = params["backend"]

celery_app = Celery(
            "report",
            backend=backend,
            broker=broker
            )

def random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

@celery_app.task
def create_report_util():
    time.sleep(5)
    random_report = {
        "name": random_string(10),
        "age": random.randint(20, 40),
        "about": random_string(100),
    }
    #update DB
    #exception handling
    return random_report

def create_report():
    random_report = create_report_util.apply_async() #apply_async
    return random_report.id