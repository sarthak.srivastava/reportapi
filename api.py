from flask import Flask, request
from flask_restplus import Api, Resource
from report_utils import create_report
from database_utils import *
from config import SECRET_KEY, authorizations
from auth import *

app = Flask(__name__)
app.config['SECRET_KEY'] = SECRET_KEY
api = Api(app, authorizations=authorizations)


#Health Check
@api.route('/health')
class Health(Resource):

    def get(self):
        return {"health": "Everything is working."}

#About API
@api.route('/about')
class About(Resource):

    def get(self):
        res  = {
            "About" : "Generate random reports using celery, rabbitMQ and flask-restplus.",
            "routes": {
                "/health" : "GET - Health Check", 
                "/about" : "GET - About", 
                "/create" : "POST - Create report",
                "/report/<string:report_id>" : "GET - Get report value or Download report",
                "/all": "GET - Get all report ids"
             } 
        }
        return res

#Get Report Value
@api.route('/report/<string:report_id>')
class ReportValue(Resource):


    @api.doc(security='apikey')
    @authentication_required
    def get(self, report_id):
        user_report = {
            "username" : request.user, 
            "report_id" : report_id
        }
        if not authorize_request(user_report):
            return {"message" : "Invalid request"}
        result = get_report(report_id)
        return result


#Create Report
@api.route('/create')
class ReportCreate(Resource):


    @api.doc(security='apikey')
    @authentication_required
    def post(self):
        report_id = create_report()
        res = dict()
        res["report_id"] = report_id
        user_report = {
            "username": request.user, 
            "report_id" : report_id
        }
        insert_report_id(report_id)
        insert_user_report(user_report)
        res["message"] = "Generating report. Track status of report using report_id."
        return res


@api.route('/my_reports')
class MyReports(Resource):

    @api.doc(security='apikey')
    @authentication_required
    def get(self):
        username = request.user
        reports = get_all_reports(username)
        return reports


@api.route('/all')
class AllReports(Resource):

    def get(self):
        reports = get_all_reports()
        return reports

@api.route('/login/<string:username>/<string:password>/')
class Login(Resource):

    def post(self, username, password):
        user = {
            "username" : username, 
            "password" : password
        }
        user = get_user(user)
        print(user)
        if user:
            user  = {
                'username' : user['username'], 
                'password' : user['password']
                }
            jtoken = encoded_token(user, app.config.get("SECRET_KEY"))
            print(decode_token(jtoken, app.config.get("SECRET_KEY")))
            return {"jwt" : jtoken, 'message' : "Authenticated"}
        
        return {'message' : 'Invalid username or password' }


@api.route('/register/<string:username>/<string:password>/')
class Register(Resource):

    def post(self, username, password):
        user = {"username":username, "password":password}
        if get_user(user):
            return {"message" : "Use already exists."}
        insert_user(user)
        return {'message' : "User added"}
        


if __name__ == '__main__':
    app.run(debug=True)