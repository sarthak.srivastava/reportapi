params = {
    "broker" : "amqp://sarthak:sarthak@localhost:5672/report_host", 
    "backend" : "mongodb://localhost/report_tasks"
}

SECRET_KEY = "sarthaksrivastava"

authorizations = {
    "apikey" : {
        'type' : 'apiKey',
        'in' : 'header', 
        'name' : 'X-API-KEY' #can be changed
    }
}

