from functools import wraps
import jwt
from flask import request
from database_utils import get_user, get_user_report
from config import SECRET_KEY
# from werkzeug.security import generate_password_hash, check_password_hash

def encoded_token(payload, SECRET_KEY):
    token = jwt.encode(payload, SECRET_KEY, algorithm='HS256')
    token = token.decode('UTF-8')
    return token

def decode_token(jtoken, SECRET_KEY):
    try:
        payload = jwt.decode(jtoken, SECRET_KEY)      
        return payload
    except jwt.ExpiredSignatureError:
        return 'Signature expired. Please log in again.'
    except jwt.InvalidTokenError:
        return 'Invalid token. Please log in again.'

def authenticate_user(jtoken, SECRET_KEY):
    user = decode_token(jtoken, SECRET_KEY)
    print("User " , user)
    user = get_user(user)
    if user:
        return user
    return None
            
def authorize_request(obj):
    print("Authorizing", obj)
    user_report = dict()
    user_report['username'] = obj['username']
    user_report['report_id'] = obj['report_id']
    if get_user_report(user_report):
        return True
    return False

def authentication_required(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        token = None
        user_obj = None
        if 'X-API-KEY' in request.headers:
            token = request.headers['X-API-KEY']
            # print(f"Received {token}")
            user_obj = authenticate_user(token, SECRET_KEY)
            if not user_obj:
                return {'message' : 'Invalid token'}
        if not token:
            return {'message' : 'Token is missing'}
        request.user = user_obj['username']
        return func(*args, **kwargs)
    return decorator