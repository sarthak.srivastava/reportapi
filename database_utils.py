from pymongo import MongoClient
import json
mongo_client = MongoClient('localhost', 27017)

db_tasks = mongo_client.report_tasks
tasks = db_tasks.celery_taskmeta

db_reports = mongo_client.report_tasks
reports = db_reports.reports
users_table = db_reports.users_table
user_reports_table = db_reports.user_reports_table

def get_report(report_id):
    result = {
            "report_id" : report_id,
            "status" : "INVALID ID"
        }
    report = tasks.find_one({
        "_id" : report_id
        }) 
    
    if report:
        report = report["result"]
        report = eval(report)
        result["report"] = report
        result["status"] = "SUCCESS" 
    else:
        report = reports.find_one({
            "_id" : report_id
            }) 
        if report:
            result["status"] = "PENDING"
    return result


def insert_report_id(report_id):
    reports.insert_one({
        "_id" : report_id
    })

def get_all_reports(user=None):
    res = list()
    if user:
        print("Finding reports of", user)
        result = user_reports_table.find({'username' : user})
        for report_id in result:
            res.append(report_id['report_id'])
    else:
        result = reports.find({})
        for report_id in result:
            res.append(report_id)
    return res

def get_user(user):
    user = users_table.find_one(user)
    print(user)
    return user

def insert_user(user):
    users_table.insert_one(user)

def insert_user_report(user_report):
    user_reports_table.insert_one(user_report)

def get_user_report(user_report):
    user_report_obj = user_reports_table.find_one(user_report)
    return user_report_obj